#
#
#
VERSION=0.1-SNAPSHOT
RAPIDMINER_HOME=/usr/local/rapidminer-5.1
STREAMS_HOME=..

STREAM_API_HOME=$(STREAMS_HOME)/stream-api

plugin:
	@echo "Building RapidMiner-Beans Plugin..."
	@mvn compile package 

install:
	@echo "Installing plugin in $(RAPIDMINER_HOME)/lib/plugins"
	@cp target/RapidMiner-Beans-Plugin-$(VERSION).jar $(RAPIDMINER_HOME)/lib/plugins

all:	plugin install

clean:
	@echo "Cleaning project directory..."
	@mvn clean >> /dev/null
