/**
 * 
 */
package com.rapidminer.beans.utils;

import java.io.File;
import java.lang.reflect.Modifier;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.annotations.Description;

import com.rapidminer.annotations.OperatorInfo;
import com.rapidminer.beans.OperatorBeanDescription;

/**
 * @author chris
 * 
 */
public class BeanFinder {

	static Logger log = LoggerFactory.getLogger(BeanFinder.class);

	final ClassLoader classLoader;

	public BeanFinder() {
		this.classLoader = BeanFinder.class.getClassLoader();
	}

	public BeanFinder(ClassLoader classLoader) {
		this.classLoader = classLoader;
	}

	public List<Class<?>> findBeans(Collection<URL> urls) throws Exception {
		List<Class<?>> beans = new ArrayList<Class<?>>();

		for (URL url : urls) {
			beans.addAll(findBeans(url));
		}

		return beans;
	}

	public List<Class<?>> findBeans(URL url) throws Exception {
		List<Class<?>> beans = new ArrayList<Class<?>>();
		File file = new File(url.getFile());

		if (file.isFile() && file.getName().endsWith(".jar")) {
			beans.addAll(findClasses(new JarFile(file)));
		} else {
			if (file.isDirectory()) {
				beans.addAll(findClasses(file, ""));
			}
		}

		return beans;
	}

	private List<Class<?>> findClasses(File directory, String packageName)
			throws ClassNotFoundException {

		log.debug("Searching directory '{}' for package '{}'", directory,
				packageName);

		List<Class<?>> classes = new ArrayList<Class<?>>();
		if (!directory.exists()) {
			return classes;
		}
		File[] files = directory.listFiles();
		if (files == null)
			return classes;

		for (File file : files) {
			if (file.isDirectory()) {
				assert !file.getName().contains(".");

				if (packageName.isEmpty()) {
					classes.addAll(findClasses(file, file.getName()));
				} else {
					classes.addAll(findClasses(file,
							packageName + "." + file.getName()));
				}
			} else if (file.getName().endsWith(".class")) {
				try {
					String className = packageName
							+ '.'
							+ file.getName().substring(0,
									file.getName().length() - 6);

					log.debug("Checking element {}", className);

					if (className.indexOf("$") >= 0) {
						continue;
					}

					while (className.startsWith("."))
						className = className.substring(1);

					log.trace("Loading class '{}'", className);
					classes.add(Class.forName(className, false, classLoader));
				} catch (NoClassDefFoundError ncdfe) {
				} catch (ClassNotFoundException cnfe) {
				} catch (Exception e) {
					log.error("Failed to add class: {}", e.getMessage());
				}
			}
		}
		return classes;
	}

	public List<Class<?>> findClasses(JarFile jar)
			throws ClassNotFoundException {
		List<Class<?>> classes = new ArrayList<Class<?>>();
		log.debug("Checking jar-file {}", jar.getName());
		Enumeration<JarEntry> en = jar.entries();
		while (en.hasMoreElements()) {

			JarEntry entry = en.nextElement();
			entry.getName();
			log.trace("Checking JarEntry '{}'", entry.getName());

			if (entry.getName().endsWith(".class")
					&& entry.getName().indexOf("$") < 0) {
				try {
					String className = entry.getName()
							.replaceAll("\\.class$", "").replaceAll("/", ".");
					log.debug("Class-name is: '{}'", className);

					Class<?> clazz = Class.forName(className, false,
							classLoader);
					log.debug("Found class {}", clazz);
					classes.add(clazz);
				} catch (NoClassDefFoundError ncdfe) {
					log.error("Warning: {}", ncdfe.getMessage());
				} catch (ClassNotFoundException cnfe) {
					log.error("Warning: {}", cnfe.getMessage());
				} catch (Exception e) {
					log.error("Failed to load class for entry '{}': {}",
							entry.getName(), e.getMessage());
					e.printStackTrace();
				}
			}
		}

		return classes;
	}

	public static List<Class<?>> findProcessors(String[] packageNames,
			ClassLoader classLoader) {

		List<Class<?>> result = new ArrayList<Class<?>>();

		for (String pkgName : packageNames) {
			result.addAll(findProcessors(pkgName, classLoader));
		}

		return result;
	}

	public static List<Class<?>> findProcessors(String pkgName,
			ClassLoader classLoader) {
		List<Class<?>> list = new ArrayList<Class<?>>();

		try {
			Class<?>[] classes = ClassFinder.getClasses(pkgName, classLoader);
			for (Class<?> clazz : classes) {

				if (clazz.isInterface()
						|| Modifier.isAbstract(clazz.getModifiers())) {
					continue;
				}

				if (clazz.isAnonymousClass()
						|| clazz.toString().indexOf("$") > 0) {
					continue;
				}

				if (!OperatorBeanDescription.canCreate(clazz))
					continue;

				Description desc = clazz.getAnnotation(Description.class);
				OperatorInfo operatorInfo = clazz
						.getAnnotation(OperatorInfo.class);
				if (desc == null && operatorInfo == null) {
					log.debug(
							"Skipping processor class '{}' due to missing Description annotation...",
							clazz);
					continue;
				}

				list.add(clazz);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		log.debug("Found {} operator-beans.", list.size());
		return list;
	}

	public static void main(String[] args) throws Exception {
		BeanFinder finder = new BeanFinder();
		List<URL> urls = new ArrayList<URL>();
		urls.add(new URL("file:/Users/chris/.RapidMiner5/beans/ExampleBean.jar"));

		List<Class<?>> beans = finder.findBeans(urls);
		log.info("Found {} beans:", beans.size());
		for (Class<?> bean : beans) {
			log.info("   {} ", bean);
		}

	}
}
