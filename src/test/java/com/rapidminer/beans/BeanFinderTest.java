/**
 * 
 */
package com.rapidminer.beans;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.PropertyConfigurator;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rapidminer.beans.utils.BeanFinder;

/**
 * @author chris
 * 
 */
public class BeanFinderTest {

	static Logger log = LoggerFactory.getLogger(BeanFinderTest.class);

	@Test
	public void test() throws Exception {

		PropertyConfigurator.configure(BeanFinderTest.class
				.getResource("/log4j.properties"));

		BeanFinder finder = new BeanFinder();
		List<URL> urls = new ArrayList<URL>();
		urls.add(new URL("file:/Users/chris/.RapidMiner5/beans/ExampleBean.jar"));

		List<Class<?>> beans = finder.findBeans(urls);
		log.info("Found {} beans:", beans.size());
		for (Class<?> bean : beans) {
			log.info("   {} ", bean);
		}
	}
}
